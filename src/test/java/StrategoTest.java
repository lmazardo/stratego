import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class StrategoTest {

    @Before
    public void before() {
        stratego = new Stratego();
    }

    @Test
    public void whenTheMarshalAttacksHeAlwaysWin() {

        assertThat(stratego.fight(Piece.MARSHAL, Piece.GENERAL)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.COLONEL)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.MAJOR)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.CAPITAIN)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.LIEUTENANT)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.SERGEANT)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.MINER)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.SCOUT)).isEqualTo(Piece.MARSHAL);
        assertThat(stratego.fight(Piece.MARSHAL, Piece.SPY)).isEqualTo(Piece.MARSHAL);
    }

    @Test
    public void ScoutWinsVersusMarshal() {
        Piece winner = stratego.fight(Piece.SCOUT, Piece.MARSHAL);
        assertThat(winner).isEqualTo(Piece.SCOUT);
    }

    @Test
    public void colonelLooseVersusMarechal() {
        Piece winner = stratego.fight(Piece.COLONEL, Piece.MARSHAL);

        assertThat(winner).isEqualTo(Piece.MARSHAL);
    }

    @Test
    public void majorWinsVersusCapitain() {
        Piece winner = stratego.fight(Piece.MAJOR, Piece.CAPITAIN);

        assertThat(winner).isEqualTo(Piece.MAJOR);
    }

    @Test
    public void lieutenantWinsVersusSergeant() {
        Piece winner = stratego.fight(Piece.LIEUTENANT, Piece.SERGEANT);

        assertThat(winner).isEqualTo(Piece.LIEUTENANT);
    }

    @Test
    public void cantWinWithSameAttackerAndDefenser() {
        Piece piece = stratego.fight(Piece.CAPITAIN, Piece.CAPITAIN);
        assertThat(piece).isNull();
    }

    @Test
    public void ColonelWinsVersusScout() {
        Piece winner = stratego.fight(Piece.COLONEL, Piece.SCOUT);

        assertThat(winner).isEqualTo(Piece.COLONEL);
    }

    private Stratego stratego;
}
