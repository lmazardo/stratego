public enum Piece {

    MARSHAL("marshal", 10),
    GENERAL("general", 9),
    COLONEL("colonel", 8),
    MAJOR("major", 7),
    CAPITAIN("capitain", 6),
    LIEUTENANT("lieutenant", 5),
    SERGEANT("sergeant", 4),
    MINER("miner", 3),
    SCOUT("scout", 2),
    SPY("spy", 1);

    public String getName() {
        return name;
    }

    public int getRank() {
        return rank;
    }

    Piece(String name, int rank) {
        this.rank = rank;
        this.name = name;
    }

    private final String name;
    private final int rank;
}
