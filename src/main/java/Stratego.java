import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Stratego {

    public Piece fight(Piece attacker, Piece defenser) {

        if (particularCase(attacker, defenser)) return attacker;

        if (attacker.getRank() > defenser.getRank()) {
            return attacker;
        } else if (attacker.getRank() < defenser.getRank()) {
            return defenser;
        }

        return null;
    }

    private boolean particularCase(Piece attacker, Piece defenser) {
        if (attacker.equals(Piece.SCOUT) && defenser.equals(Piece.MARSHAL)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Stratego stratego = new Stratego();
        Piece scout = Piece.SCOUT;
        Piece capitain = Piece.CAPITAIN;

        Logger logger = LoggerFactory.getLogger("main");
        logger.debug("test");
        System.out.println(capitain.getName() + " attacks " + scout.getName());
        Piece winner = stratego.fight(capitain, scout);
        System.out.println("winner is : " + winner.getName());

    }

}
